package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

public class HashTableOA<K, V> implements Map<K, V> {
	
	public static class MapEntry<K, V> {
		private K key;
		private V value;
		
		public K getKey() {
			return this.key;
		}
		
		public V getValue() {
			return this.value;
		}
		
		public void setKey(K key) {
			this.key = key;
		}
		
		public void setValue(V value) {
			this.value = value;
		}
		
		public MapEntry(K key, V value) {
			//super();
			this.key = key;
			this.value = value;
		}
		
	}
	
	private int currentSize;
	private Object buckets[];
	private static final int DEFAULT_BUCKETS = 11;
	private Comparator<V> valueComparator;
	private Comparator<K> keyComparator;
	
	private int generateHash(int key) {
		return key % this.buckets.length;
	}
	
	private int hashFunction(K key) {
		//Normal hash
		int targetBucket = Math.abs(generateHash(key.hashCode()));
		boolean isBucketInUse = ((CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket]).isInUse();
		
		if(!isBucketInUse)
			return targetBucket;
		
		//Hash^2
		targetBucket = Math.abs(generateHash(key.hashCode() * key.hashCode()));
		isBucketInUse = ((CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket]).isInUse();
		
		
		
		//While the bucket is in use go on the loop until the bucket is NOT in use and i is less than the buckets[] length
		for(int i = 0; isBucketInUse && i < this.buckets.length - 1; i++) {
			targetBucket = Math.abs(generateHash(key.hashCode() + i));
			isBucketInUse = ((CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket]).isInUse();
			
		}

		return targetBucket;
	}
	
	
	public HashTableOA(int numBuckets, Comparator<V> valComparator, Comparator<K> keyComparator) {
		if(numBuckets < 1) {
			throw new IllegalArgumentException("Bucket size needs to be at least 1");
		}
		
		this.currentSize = 0;
		this.buckets = new Object[numBuckets];
		this.valueComparator = valComparator;
		this.keyComparator = keyComparator;
		
		//Create the buckets with a list on each one.
		for(int i = 0; i < this.buckets.length; i++) {
			this.buckets[i] = new CircularSortedDoublyLinkedList<MapEntry<K, V>>(valComparator);
		}
	}
	
	public HashTableOA(Comparator<V> valComparator, Comparator<K> keyComparator) {
		this.currentSize = 0;
		this.buckets = new Object[DEFAULT_BUCKETS];
		this.valueComparator = valComparator;
		this.keyComparator = keyComparator;
		
		//Create the buckets with a list on each one.
		for(int i = 0; i < this.buckets.length; i++) {
			this.buckets[i] = new CircularSortedDoublyLinkedList<MapEntry<K, V>>(valComparator);
		}
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize < 1;
	}

	@Override
	public V get(K key) {
		if(key == null)
			throw new IllegalArgumentException("key cannot be null");
		
		if(this.isEmpty()) {
			return null;
		}
		
		
		
		int targetBucket = Math.abs(generateHash(key.hashCode()));
		CircularSortedDoublyLinkedList<MapEntry<K, V>> L = (CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket];
		
		//Best/Avg case scenario O(1)
		//if L is empty and you use equals is going to throw a null pointer
		if(!L.isEmpty() && key.equals(L.first().getKey())) {
			return L.first().getValue();
		}
		
		
		//Hash^2
		targetBucket = Math.abs(generateHash(key.hashCode() * key.hashCode()));
		L = ((CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket]);
		if(!L.isEmpty() && key.equals(L.first().getKey())) {
			return L.first().getValue();
		}
		
		//Worst case scenario; probing until you get the right key, if not then the value is not in the hashtable O(n)
		for(int i = 0; i < this.buckets.length - 1; i++) {
			
			targetBucket = Math.abs(generateHash(key.hashCode()+i));
			
			CircularSortedDoublyLinkedList<MapEntry<K, V>> Lista = (CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket];
			
			if(!Lista.isEmpty() && key.equals(Lista.first().getKey())) {
				V valueToReturn = Lista.first().getValue();
				return valueToReturn;
			}
		}
		
		return null;
	}

	@Override
	public V put(K key, V value) {
		if(key == null)
			throw new IllegalArgumentException("key cannot be null");
		if(value == null)
			throw new IllegalArgumentException("value cannot be null");

		if(this.size() == this.buckets.length)
			this.reAllocate();
		

		//If there is already a key, remove that key and put the new one
		if(this.contains(key)) {
			this.remove(key);
		}
		
		int targetBucket = this.hashFunction(key);
		MapEntry<K , V> newMapEntry = new MapEntry<K , V>(key, value);
		
		//Find the list where the MapEntry is going to be added.
		CircularSortedDoublyLinkedList<MapEntry<K, V>> L = (CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket];
		
		L.add(newMapEntry);
		this.currentSize++;
		
		return value;
	}

	@Override
	public V remove(K key) {
		if(key == null)
			throw new IllegalArgumentException("key cannot be null");
		
		
		int targetBucket = Math.abs(generateHash(key.hashCode()));
		CircularSortedDoublyLinkedList<MapEntry<K, V>> L = (CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket];
		
		//Best/Avg case scenario O(1) 
		if(L != null && L.first().getKey().equals(key)) {
			V valueToReturn = L.first().getValue();
			L.remove(L.first());
			
			this.currentSize--;
			
			return valueToReturn;
		}
		
		//Hash^2
		targetBucket = Math.abs(generateHash(key.hashCode() * key.hashCode()));
		L = ((CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket]);
		
		if(L != null && L.first().getKey().equals(key)) {
			V valueToReturn = L.first().getValue();
			L.remove(L.first());
			
			this.currentSize--;
			
			return valueToReturn;
		}
		
		
		
		
		//Worst case scenario O(n); probing until you get the right key, if not then the value is not in the hashtable
		for(int i = 0; i < this.buckets.length - 1; i++) {
			targetBucket = Math.abs(generateHash(key.hashCode() + i));
			L = (CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket];
			
			if(L != null && L.first().getKey().equals(key)) {
				V valueToReturn = L.first().getValue();
				L.remove(L.first());
				
				this.currentSize--;
				
				return valueToReturn;
			}
			
		}
		
		return null;
	}

	@Override
	public boolean contains(K key) {
		if(this.get(key) != null)
			return true;
		else
			return false;
	}

	@Override
	public SortedList<K> getKeys() {
		SortedList<K> result = (SortedList<K>) new CircularSortedDoublyLinkedList<K>(this.keyComparator);
		for(Object o: this.buckets) {
			for(MapEntry<K, V> M: (CircularSortedDoublyLinkedList<MapEntry<K, V>>) o) {
				result.add(M.getKey());
			}
		}
		
		return result;
	}

	@Override
	public SortedList<V> getValues() {
		SortedList<V> result = new CircularSortedDoublyLinkedList<V>(this.valueComparator);
		for(Object o: this.buckets) {
			for(MapEntry<K, V> M: (CircularSortedDoublyLinkedList<MapEntry<K, V>>) o) {
				result.add(M.getValue());
			}
		}
		
		return result;
	}
	
	
	//If you reallocate tienes que rehash xq el bucket length no sera el mismo.
	private void reAllocate() {
		Object[] newBucket = new Object[this.buckets.length*2];
		
		for(int i = 0; i < newBucket.length; i++) {
			newBucket[i] = new CircularSortedDoublyLinkedList<MapEntry<K, V>>(this.valueComparator);
		}
		
		for(Object o: this.buckets) {
			for(MapEntry<K, V> M: (CircularSortedDoublyLinkedList<MapEntry<K, V>>) o) {
				int targetBucket = this.reHashFunction(M.getKey(), newBucket.length, newBucket);
				CircularSortedDoublyLinkedList<MapEntry<K, V>> L = (CircularSortedDoublyLinkedList<MapEntry<K, V>>) newBucket[targetBucket];
				L.add(M);
			}
		}
		
		this.buckets = newBucket;
		
		
	}
	
	private int reHash(int key, int newBucketsSize) {
		return key % newBucketsSize;
	}
	
	private int reHashFunction(K key, int newBucketsSize, Object[] newBucket) {
		//Normal hash
		int targetBucket = Math.abs(reHash(key.hashCode(), newBucketsSize));
		boolean isBucketInUse = ((CircularSortedDoublyLinkedList<MapEntry<K, V>>) newBucket[targetBucket]).isInUse();
		
		if(!isBucketInUse)
			return targetBucket;
		
		//Hash^2
		targetBucket = Math.abs(reHash(key.hashCode() * key.hashCode(), newBucketsSize));
		isBucketInUse = ((CircularSortedDoublyLinkedList<MapEntry<K, V>>) this.buckets[targetBucket]).isInUse();
		
		//Linear Probing
		//While the bucket is in use go on the loop until the bucket is NOT in use and i is less than the buckets[] length
		for(int i = 0; isBucketInUse && i < newBucket.length - 1; i++) {
			targetBucket = Math.abs(reHash(key.hashCode() + i, newBucketsSize));
			isBucketInUse = ((CircularSortedDoublyLinkedList<MapEntry<K, V>>) newBucket[targetBucket]).isInUse();
			
		}

		return targetBucket;
	}
	
	public void makeEmpty() {
		for(int i = 0; i < this.buckets.length; i++) {
			this.buckets[i] = new CircularSortedDoublyLinkedList<MapEntry<K, V>>(this.valueComparator);
		}
		this.currentSize = 0;
		
	}

}
