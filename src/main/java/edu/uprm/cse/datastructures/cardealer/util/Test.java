package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.comparators.comparator;
import edu.uprm.cse.datastructures.cardealer.model.Car;

public class Test {

	public static void main(String[] args) {
//		public Car(long carId, String carBrand, String carModel, String carModelOption, double carPrice) {
		SortedList<Car> ddl = new CircularSortedDoublyLinkedList<Car>(new comparator());
		Comparator<Integer> compara = new IntegerComparator();
		
		HashTableOA<Integer, Car> hashtable = new HashTableOA<Integer, Car>(5, new comparator(), compara);
		
		Car zero = new Car(1, "Lamborghini", "Aventador", "A", 100000);
		Car uno = new Car(2, "Lamborghini", "Aventador", "AB", 200000);
		Car dos = new Car(3, "Lamborghini", "Aventador", "ABC", 300000);
		Car tres = new Car(4, "Lamborghini", "Aventador", "ABCD", 400000);
		Car cuatro = new Car(5, "Lamborghini", "Aventador", "ABCDE", 500000);
		
		
		Car cinco = new Car(6, "Lamborghini", "Aventador", "ABCDE", 500000);
		Car seis = new Car(7, "Lamborghini", "Aventador", "ABCDE", 500000);
		Car siete = new Car(8, "Lamborghini", "Aventador", "ABCDE", 500000);
		Car ocho = new Car(9, "Lamborghini", "Aventador", "ABCDE", 500000);
		Car nueve = new Car(10, "Lamborghini", "Aventador", "ABCDE", 500000);
		Car diez = new Car(11, "Lamborghini", "Aventador", "ABCDE", 500000);
		Car once = new Car(12, "Lamborghini", "Aventador", "ABCDE", 500000);
		Car doce = new Car(13, "Lamborghini", "Aventador", "ABCDE", 500000);
		Car trece = new Car(14, "Lamborghini", "Aventador", "ABCDEFGHIJ", 500000);
		
		
		hashtable.put(10, nueve);
		hashtable.put(11, diez);
		hashtable.put(12, once);
		hashtable.put(13, doce);
		hashtable.put(14, trece);
		hashtable.put(4, tres);
		hashtable.put(1, zero);
		hashtable.put(3, dos);
		hashtable.put(2, uno);
		
		hashtable.put(6, cinco);
		hashtable.put(7, seis);
		hashtable.put(8, siete);
		hashtable.put(9, ocho);
	
		
		
		
		
		System.out.println(hashtable.get(2));
		
		
//		System.out.println(hashtable.get(113));
		
		SortedList<Car> test = hashtable.getValues();
		
		
		
		
		
		for(Car c: test) {
			System.out.println(c);
		}
		System.out.println("Hash table size is: " + hashtable.size());
//		hashtable.makeEmpty();
		
		hashtable.put(4, tres);
		hashtable.put(1, zero);
		hashtable.put(3, dos);
		hashtable.put(2, uno);
		
		System.out.println();
		System.out.println("After makeEmpty() and added the cars again");
		System.out.println();
		
		test = hashtable.getValues();
				
				
				
				
				
				for(Car c: test) {
					System.out.println(c);
				}
		
		System.out.println("Hash table size is: " + hashtable.size());
		
		
		/* ddl.add(new Car(123, "Lamborghini", "Aventador", "ABC", 600000));
		ddl.add(zero); 
		ddl.add(new Car(113, "Lamborghini", "Aventador", "ABCD", 500000));
		ddl.add(uno);
        ddl.add(uno);
        ddl.add(zero);
		ddl.add(new Car(123, "Lamborghini", "Aventador", "ABCDE", 600000)); */
		
//		int counter = 0;
//		
//		while(counter < ddl.size()) {
//			System.out.println(ddl.get(counter));
//			counter++;
//		}
		
		
//		System.out.println(ddl.firstIndex(zero));
//		System.out.println(ddl.lastIndex(zero));
	
//		System.out.println(ddl.remove(uno));
//		System.out.println(ddl.remove(zero));
		
		//counter = 0;
		
//		while(counter < ddl.size()) {
//			System.out.println(ddl.get(counter));
//			counter++;
//		}
//		
//		counter = 0;
//		
//		ddl.clear();
//		System.out.println(ddl.size());
//		
//		while(counter < ddl.size()) {
//			System.out.println(ddl.get(counter));
//			counter++;
//		}
		

	}
	
	public static class IntegerComparator implements Comparator<Integer> {
		
		public IntegerComparator() {
			
		}

		@Override
		public int compare(Integer o1, Integer o2) {
			return o1.compareTo(o2);
		}
		
	}

}


