package edu.uprm.cse.datastructures.cardealer.comparators;

import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class comparator implements Comparator<Car> {

	@Override
	public int compare(Car car1, Car car2) {
		String carro1 = car1.getCarBrand() + car1.getCarModel() + car1.getCarModelOption();
		String carro2 = car2.getCarBrand() + car2.getCarModel() + car2.getCarModelOption();
		
		return carro1.compareTo(carro2);
	}

}
