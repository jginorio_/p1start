package edu.uprm.cse.datastructures.cardealer.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response; 

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/")
public class CarServices {
	
	private static HashTableOA<Integer, Car> carList = CarTable.getInstance();
	
	//Add a car
	@POST
	@Path("/cars/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addCar(Car carToAdd) {
		carList.put((int)carToAdd.getCarId(), carToAdd);
		//HTTP 201
		return Response.status(201).build();
	}
	
	
	//Get a specific car using the ID.
	@GET
	@Path("/cars/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCarByID(@PathParam("id") long id) {
		Car carToReturn = carList.get((int)id);
		
		if(carToReturn != null)
			return carToReturn;
		
		
		//Because is expecting a Car object in return not a response.
		throw new WebApplicationException(404);
	}
	
	//TODO: FIX THIS
	//Get all cars.
	@GET
	@Path("/cars")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		
		Car[] result = new Car[carList.size()];
		int counter = 0;
		for(Car c: carList.getValues()) {
			result[counter] = c;
			counter++;
		}
		
		
		return result;
	}
	
	
	//Update specific car.
	@PUT
	@Path("/cars/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCarByID(@PathParam("id") long id, Car car) {
		
		if(carList.contains((int)id)) {
			carList.put((int)id, car);
			//HTTP 200
			return Response.status(Response.Status.OK).build();
		}
		
		//HTTP 404 
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	
	@DELETE
	@Path("/cars/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCarByID(@PathParam("id") long id) {

		
		if(carList.remove((int)id) != null) {
			//HTTP 200
			return Response.status(Response.Status.OK).build();
		}

		//HTTP 404
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
