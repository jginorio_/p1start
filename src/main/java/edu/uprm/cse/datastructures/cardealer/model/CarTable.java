package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.comparators.comparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {
	private static HashTableOA<Integer, Car> cList = new HashTableOA<Integer, Car>(new comparator(), new IntegerComparator());
	private CarTable(){}
	
	public static HashTableOA<Integer, Car> getInstance(){
	    return cList;
	  }
	
	public static void resetCars() {
		cList.makeEmpty();
	}
	
	private static class IntegerComparator implements Comparator<Integer> {
		
		public IntegerComparator() {}

		@Override
		public int compare(Integer o1, Integer o2) {
			return o1.compareTo(o2);
		}
		
	}
	
}
